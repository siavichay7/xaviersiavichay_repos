import { Exclude } from 'class-transformer';
import { Organization } from 'src/organization/entities/organization.entity';
import { Repository } from 'src/repository/entities/repository.entity';
import { PrimaryGeneratedColumn, Column, Entity, OneToMany, JoinColumn, ManyToOne, UpdateDateColumn, DeleteDateColumn } from 'typeorm';

@Entity("tribe")
export class Tribe {
    @PrimaryGeneratedColumn({name: 'id_tribe'})
    idTribe: number; 

    @Column({ type: 'varchar' })
    name: string;

    @Column({ type: 'varchar' })
    status: string;

    @Column({ type: 'int', name: 'id_organization' })
    idOrganization: number;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    date: Date;

    @Exclude()
    @UpdateDateColumn({ name: 'update_date' })
    updateDate: Date;

    @Exclude()
    @DeleteDateColumn({ name: 'delete_date' })
    deleteDate: Date;

    @ManyToOne(() => Organization, (organization) => organization.tribes)
    @JoinColumn({ name: 'id_organization', referencedColumnName: 'idOrganization' })
    organization: Organization;

    @OneToMany(() => Repository, (repository) => repository.tribe)
    repositories: Repository[];

}
