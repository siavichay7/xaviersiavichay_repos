import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';

@Injectable()
export class MockService {

    mocks = {
        "repositories": [
            {
            "id": 1,
            "state": 604
            },
            {
            "id": 2,
            "state": 605
            },
            {
            "id": 3,
            "state": 606
            }
            ]
    } 

    constructor(
    ) { }

    //TODO: MOCKS

    async findMocks() {
        return this.mocks;
    }

}
