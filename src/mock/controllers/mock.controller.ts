import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, ParseIntPipe, Post, Put, Req, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { IOperationError } from 'src/providers/IOperationResult';
import { TransformInterceptor } from 'src/providers/transform.interceptor';
import { MockDto } from '../dtos/mock.dto';
import { MockService } from '../services/mock.service';


@ApiTags('Mocks')
@Controller('')
export class MockController {
    constructor(private mockService: MockService) { }
    // MOCKS

    @UseInterceptors(TransformInterceptor)
    @ApiResponse({ status: 200, description: 'Success.', type: [MockDto] })
    @ApiResponse({
        status: 500, description: 'Server error.', type: IOperationError
    })
    @Get('mocks')
    getOrganizations() {
        return this.mockService.findMocks();
    }
}
