import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { LessThan, MoreThan, Repository } from 'typeorm';
import { Repository as Repos } from 'src/repository/entities/repository.entity' ;
import { RepositoryDto } from '../dtos/repository.dto';
var fs = require('fs');
var stringify = require('csv-stringify');
import { MockService } from '../../mock/services/mock.service';

@Injectable()
export class RepositoryService {
    constructor(
        @InjectRepository(Repos) private repositoryRepo: Repository<Repos>,
        public mockService: MockService
    ) { }

    //TODO: REPOSITORIES

    async findRepositoriesByIdTribe(idTribe, params): Promise<RepositoryDto[]> {        
        //TODO: VALIDO SI EXISTE PARAMS
        if (params != null) {
            var { date, coverage, state } = params;        
        }
        const tribe = await this.repositoryRepo.findOne(idTribe);
        //TODO: VALIDO SI EXISTE TRIBE
        if (!tribe) {
            throw new NotFoundException(`La Tribu no se encuentra registrada`);
        }
        const repositories: Repos[] = await this.repositoryRepo.find(
            {
                where: [
                {idTribe: idTribe, stateChar: 'E'},
                {date: date},
                {stateChar: state},
                {date: MoreThan("2022-01-01")},
                {date: LessThan("2022-12-30")}
            ] , 
            relations:['metrics'] }
        );
        //TODO: VALIDO EL ESTADO DE MI REPOSITORY
        var resultRepository: Repos[];
            var mocks = await this.mockService.findMocks();
            //TODO: VALIDO SI EXISTE COVERAGE
            if (coverage) {
                resultRepository = repositories.filter(res=> res.metrics.filter(met => met.coverage > 75.00 && met.coverage == coverage))
            } else {
                resultRepository = repositories.filter(res=> res.metrics.filter(met => met.coverage > 75.00))
            }
            if (!resultRepository) {
                throw new NotFoundException(`La Tribu no tiene repositorios que cumplan con la cobertura necesaria`);
            }
            resultRepository.forEach(element => {
                switch (element.idRepository) {
                    case 1:
                        element.verificationState = "Verificado"
                        break;
                    case 2:
                        element.verificationState = "En Espera"
                        break;
                    case 3:
                        element.verificationState = "Aprobado"
                        break;
                    default:
                        break;
                }
            });
        return resultRepository.map((repository: Repos) => plainToClass(RepositoryDto, repository))
    }

    async findCsvByIdTribe(idTribe, params): Promise<RepositoryDto[]> {
         //TODO: VALIDO SI EXISTE PARAMS
         if (params != null) {
            var { date, coverage, state } = params;        
        }
        const tribe = await this.repositoryRepo.findOne(idTribe);
        //TODO: VALIDO SI EXISTE TRIBE
        if (!tribe) {
            throw new NotFoundException(`La Tribu no se encuentra registrada`);
        }
        const repositories: Repos[] = await this.repositoryRepo.find(
            {
                where: [
                {idTribe: idTribe, stateChar: 'E'},
                {date: date},
                {stateChar: state},
                {date: MoreThan("2022-01-01")},
                {date: LessThan("2022-12-30")}
            ] , 
            relations:['metrics'] }
        );
        //TODO: VALIDO EL ESTADO DE MI REPOSITORY
        var resultRepository: Repos[];
            var mocks = await this.mockService.findMocks();
            //TODO: VALIDO SI EXISTE COVERAGE
            if (coverage) {
                resultRepository = repositories.filter(res=> res.metrics.filter(met => met.coverage > 75.00 && met.coverage == coverage))
            } else {
                resultRepository = repositories.filter(res=> res.metrics.filter(met => met.coverage > 75.00))
            }
            if (!resultRepository) {
                throw new NotFoundException(`La Tribu no tiene repositorios que cumplan con la cobertura necesaria`);
            }
            resultRepository.forEach(element => {
                switch (element.idRepository) {
                    case 1:
                        element.verificationState = "Verificado"
                        break;
                    case 2:
                        element.verificationState = "En Espera"
                        break;
                    case 3:
                        element.verificationState = "Aprobado"
                        break;
                    default:
                        break;
                }
            });
            //TODO: IMPORTO EL JSON COMO ARCHIVO CSV
        stringify(resultRepository, {
            header: true
        }, function (err, output) {
            fs.writeFile(__dirname+'/repositories.csv', output);
        })
        return resultRepository.map((repository: Repos) => plainToClass(RepositoryDto, repository))        
    }
 
}
