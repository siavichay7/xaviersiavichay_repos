import { Exclude } from 'class-transformer';
import { Metric } from 'src/metric/entities/metric.entity';
import { Tribe } from 'src/tribe/entities/tribe.entity';
import { PrimaryGeneratedColumn, Column, Entity, OneToMany, JoinColumn, ManyToOne, UpdateDateColumn, DeleteDateColumn } from 'typeorm';

@Entity("repository")
export class Repository {
    @PrimaryGeneratedColumn({name: 'id_repository', })
    idRepository: number;

    @Column({ type: 'varchar', nullable: false })
    name: string;

    @Column({ type: 'varchar', nullable: false })
    stateChar: string;

    @Column({ type: 'varchar', nullable: false })
    statusChar: string;

    @Column({ type: 'int', nullable: false, name: 'id_tribe' })
    idTribe: number;

    @Column({ type: 'varchar', nullable: false })
    verificationState: string;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    date: Date;

    @Exclude()
    @UpdateDateColumn({ name: 'update_date' })
    updateDate: Date;

    @Exclude()
    @DeleteDateColumn({ name: 'delete_date' })
    deleteDate: Date;

    @ManyToOne(() => Tribe, (tribe) => tribe.repositories)
    @JoinColumn({ name: 'id_tribe', referencedColumnName: 'idTribe' })
    tribe: Tribe;

    @OneToMany(() => Metric, (metric) => metric.repository)
    metrics: Metric[];
}
