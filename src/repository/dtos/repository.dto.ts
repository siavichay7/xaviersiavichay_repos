import { ApiProperty } from "@nestjs/swagger";
import { Exclude, Expose } from "class-transformer";
import { IsDate, IsDecimal, IsNumber, IsOptional, IsString } from "class-validator";
import { Metric } from "src/metric/entities/metric.entity";

@Exclude()
export class RepositoryDto {
    @Expose()
    @ApiProperty()
    readonly idRepository: number;

    @Expose()
    @ApiProperty()
    readonly name: number;

    @Expose()
    @ApiProperty()
    readonly stateChar: string;

    @Expose()
    @ApiProperty()
    readonly statusChar: string;

    @Expose()
    @ApiProperty()
    readonly idTribe: number;

    @Expose()
    @ApiProperty()
    readonly verificationState: string;

    @Expose()
    @ApiProperty()
    readonly metrics:Metric[];
}

export class FilterRepositoryDto {

    @ApiProperty({ required: false })
    @IsOptional()
    @IsDate()
    @ApiProperty()
    date: Date;

    @ApiProperty({ required: false })
    @IsOptional()
    @IsString()
    state: string;

    @ApiProperty({ required: false })
    @IsOptional()
    @IsNumber()
    coverage: number;
}
