import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, ParseIntPipe, Post, Put, Query, Req, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { IOperationError } from 'src/providers/IOperationResult';
import { TransformInterceptor } from 'src/providers/transform.interceptor';
import { FilterRepositoryDto, RepositoryDto } from '../dtos/repository.dto';
import { RepositoryService } from '../services/repository.service';

@ApiTags('Repository')
@Controller('')
export class RepositoryController {
    constructor(private repositoryService: RepositoryService) { }

    @UseInterceptors(TransformInterceptor)
    @ApiResponse({ status: 200, description: 'Success.', type: [RepositoryDto] })
    @ApiResponse({
        status: 500, description: 'Server error.', type: IOperationError
    })
    @Get('repositories/:idTribe')
    getRepositoriesByIdTribe(@Param('idTribe') idTribe: number,  @Query() params?: FilterRepositoryDto) {
        return this.repositoryService.findRepositoriesByIdTribe(idTribe, params);
    }

    
    @UseInterceptors(TransformInterceptor)
    @ApiResponse({ status: 200, description: 'Success.', type: [RepositoryDto] })
    @ApiResponse({
        status: 500, description: 'Server error.', type: IOperationError
    })
    @Get('repositories-csv/:idTribe')
    getCsvByIdTribe(@Param('id') idTribe: number,  @Query() params?: FilterRepositoryDto) {
        return this.repositoryService.findCsvByIdTribe(idTribe, params);
    }

}
