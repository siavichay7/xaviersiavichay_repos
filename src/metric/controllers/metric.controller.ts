import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, ParseIntPipe, Post, Put, Req, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { IOperationError } from 'src/providers/IOperationResult';
import { TransformInterceptor } from 'src/providers/transform.interceptor';
import { MetricDto } from '../dtos/metric.dtos';
import { MetricService } from '../services/metric.service';


@ApiTags('Metric')
@Controller('')
export class MetricController {
    constructor(private metricService: MetricService) { }

    @UseInterceptors(TransformInterceptor)
    @ApiResponse({ status: 200, description: 'Success.', type: [MetricDto] })
    @ApiResponse({
        status: 500, description: 'Server error.', type: IOperationError
    })
    @Get('metrics/:idRepository')
    getMetricsByIdRepository(@Param('id') idRepository: number) {
        return this.metricService.findMetricsByIdRepository(idRepository);
    }

}
