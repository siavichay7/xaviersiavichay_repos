import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { MetricDto } from '../dtos/metric.dtos';
import { Metric } from '../entities/metric.entity';


@Injectable()
export class MetricService {
    constructor(
        @InjectRepository(Metric) private metricRepo: Repository<Metric>,
    ) { }

    //TODO: METRICS

    async findMetricsByIdRepository(idRepository): Promise<MetricDto[]> {
        const metrics: Metric[] = await this.metricRepo.find(idRepository);
        return metrics.map((metric: Metric) => plainToClass(MetricDto, metric))
    }

 
}
