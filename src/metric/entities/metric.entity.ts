import { Exclude } from 'class-transformer';
import { Repository } from 'src/repository/entities/repository.entity';
import { Tribe } from 'src/tribe/entities/tribe.entity';
import { PrimaryGeneratedColumn, Column, Entity, OneToMany, JoinColumn, ManyToOne, UpdateDateColumn, DeleteDateColumn } from 'typeorm';

@Entity("metrics")
export class Metric {
    @PrimaryGeneratedColumn({name: 'id_repository', })
    idRepository: number;

    @Column({ type: 'decimal', nullable: false })
    coverage: number;

    @Column({ type: 'int', nullable: false })
    bugs: string;

    @Column({ type: 'int', nullable: false })
    vulnerabilities: number;

    @Column({ type: 'int', nullable: false })
    hotspot: number;

    @Column({ type: 'int', nullable: false })
    codeSmells: number;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    date: Date;

    @Exclude()
    @UpdateDateColumn({ name: 'update_date' })
    updateDate: Date;

    @Exclude()
    @DeleteDateColumn({ name: 'delete_date' })
    deleteDate: Date;

    @ManyToOne(() => Repository, (repository) => repository.metrics)
    @JoinColumn({ name: 'id_repository', referencedColumnName: 'idRepository' })
    repository: Repository;

}
