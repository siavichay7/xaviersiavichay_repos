import { ApiProperty } from "@nestjs/swagger";
import { Exclude, Expose } from "class-transformer";

@Exclude()
export class MetricDto {
    @Expose()
    @ApiProperty()
    readonly idRepository: number;

    @Expose()
    @ApiProperty()
    readonly coverage: number;

    @Expose()
    @ApiProperty()
    readonly bugs: string;

    @Expose()
    @ApiProperty()
    readonly vulnerabilities: number;

    @Expose()
    @ApiProperty()
    readonly hotspot: number;

    @Expose()
    @ApiProperty()
    readonly codeSmells: number;
}
