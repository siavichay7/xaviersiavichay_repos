import { Exclude } from 'class-transformer';
import { Tribe } from 'src/tribe/entities/tribe.entity';
import { PrimaryGeneratedColumn, Column, Entity, OneToMany, JoinColumn, ManyToOne, UpdateDateColumn, DeleteDateColumn } from 'typeorm';

@Entity("organization")
export class Organization {
    @PrimaryGeneratedColumn({name: 'id_organization'})
    idOrganization: number;

    @Column({ type: 'varchar' })
    name: string;

    @Column({ type: 'varchar' })
    status: string;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    date: Date;

    @Exclude()
    @UpdateDateColumn({ name: 'update_date' })
    updateDate: Date;

    @Exclude()
    @DeleteDateColumn({ name: 'delete_date' })
    deleteDate: Date;

    @OneToMany(() => Tribe, (tribe) => tribe.organization)
    tribes: Tribe[];

} 
