import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Metric } from 'src/metric/entities/metric.entity';
import { MockController } from 'src/mock/controllers/mock.controller';
import { MockService } from 'src/mock/services/mock.service';
import { RepositoryController } from 'src/repository/controllers/repository.controller';
import { Repository } from 'src/repository/entities/repository.entity';
import { RepositoryService } from 'src/repository/services/repository.service';
import { Tribe } from 'src/tribe/entities/tribe.entity';
import { OrganizationController } from './controllers/organization.controller';
import { Organization } from './entities/organization.entity';
import { OrganizationService } from './services/organization.service';



@Module({
    imports: [TypeOrmModule.forFeature([Organization, Repository, Tribe, Metric])],
    controllers: [OrganizationController, MockController, RepositoryController],
    providers: [OrganizationService, MockService, RepositoryService],
    exports: [OrganizationService, MockService, RepositoryService],
})
export class OrganizationModule { }
