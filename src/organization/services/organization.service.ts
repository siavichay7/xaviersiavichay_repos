import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { CreateOrganizationDto, OrganizationDto, UpdateOrganizationDto } from '../dtos/organization.dtos';
import { Organization } from '../entities/organization.entity';

@Injectable()
export class OrganizationService {
    constructor(
        @InjectRepository(Organization) private organizationRepo: Repository<Organization>,
    ) { }

    //TODO: ORGANIZATION

    async findOrganizations(): Promise<OrganizationDto[]> {
        const organizations: Organization[] = await this.organizationRepo.find();
        return organizations.map((organization: Organization) => plainToClass(OrganizationDto, organization))
    }

    async createOrganization(data: CreateOrganizationDto): Promise<OrganizationDto> {
        const nuevoDato = await this.organizationRepo.create(data);
        const save: Organization = await this.organizationRepo.save(nuevoDato);
        return plainToClass(OrganizationDto, save)
    }

    async updateOrganization(id: number, changes: UpdateOrganizationDto) {
        const organization = await this.organizationRepo.findOne({where: {idOrganization: id}});
        this.organizationRepo.merge(organization, changes);
        const save: Organization = await this.organizationRepo.save(organization);
        return plainToClass(OrganizationDto, save)
    }

    async deleteOrganization(id: number): Promise<string> {
        await this.organizationRepo.softDelete(id);
        return "Organization was eliminated"
    }

}
