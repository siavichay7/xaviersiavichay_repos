import { ApiProperty, PartialType } from "@nestjs/swagger";
import { Exclude, Expose, Type } from "class-transformer";
import { IsDate, IsNotEmpty, IsNumber, IsOptional, IsString, IsUrl } from "class-validator";

@Exclude()
export class OrganizationDto {
    @Expose()
    @ApiProperty()
    readonly idOrganization: number;

    @Expose()
    @ApiProperty()
    readonly name: string;

    @Expose()
    @ApiProperty()
    readonly status: string;
}

export class CreateOrganizationDto {
    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly name: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly status: string;
}

export class UpdateOrganizationDto extends PartialType(CreateOrganizationDto) { }


