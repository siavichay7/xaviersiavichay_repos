import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, ParseIntPipe, Post, Put, Req, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { IOperationError } from 'src/providers/IOperationResult';
import { TransformInterceptor } from 'src/providers/transform.interceptor';
import { CreateOrganizationDto, OrganizationDto, UpdateOrganizationDto } from '../dtos/organization.dtos';
import { OrganizationService } from '../services/organization.service';


@ApiTags('Organization')
@Controller('')
export class OrganizationController {
    constructor(private organizationService: OrganizationService) { }
    // PROMOCIONES

    @UseInterceptors(TransformInterceptor)
    @ApiResponse({ status: 200, description: 'Success.', type: [OrganizationDto] })
    @ApiResponse({
        status: 500, description: 'Server error.', type: IOperationError
    })
    @Get('organizations')
    getOrganizations() {
        return this.organizationService.findOrganizations();
    }

    @UseInterceptors(TransformInterceptor)
    @ApiResponse({
        status: 500, description: 'Server error.', type: IOperationError
    })
    @Post('organization')
    postOrganization(@Body() payload: CreateOrganizationDto) {
        return this.organizationService.createOrganization(payload);
    }

    @UseInterceptors(TransformInterceptor)
    @ApiResponse({
        status: 500, description: 'Server error.', type: IOperationError
    })
    @Put('organization/:id')
    putOrganization(@Param('id') id: number, @Body() payload: UpdateOrganizationDto) {
        return this.organizationService.updateOrganization(id, payload);
    }

    @UseInterceptors(TransformInterceptor)
    @ApiResponse({
        status: 500, description: 'Server error.', type: IOperationError
    })
    @Delete('organization/:id')
    deleteOrganization(@Param('id') id: number) {
        return this.organizationService.deleteOrganization(id);
    }


}
