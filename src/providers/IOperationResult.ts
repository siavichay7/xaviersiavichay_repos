import {
    IsString,
    IsNotEmpty,
} from 'class-validator';
import { PartialType, ApiProperty } from '@nestjs/swagger';
import { HttpStatus } from '@nestjs/common';
import { extend } from 'joi';

export class IOperationResult
    <T>  {
    @ApiProperty()
    result: T

    @ApiProperty()
    message: string

    @ApiProperty()
    success: boolean

    @ApiProperty()
    statusCode: HttpStatus
}

export class IOperationError {
    @ApiProperty({ description: 'RETORNA EL MENSAJE DE ERROR' })
    message: string

    @ApiProperty({ description: 'CUANDO OCURRE UN ERROR RETORNA FALSE' })
    success: boolean = false

    @ApiProperty({ description: 'RETORNA EL STATUS DE LA PETICIÓN' })
    statusCode: HttpStatus
}

